# Introduction

Core Plot is a 2D plotting framework for Mac OS X and iOS. It is highly customizable and capable of drawing many types of plots. See the  [Example Graphs](https://github.com/core-plot/core-plot/wiki/Example-Graphs) wiki page and the [example applications](https://github.com/core-plot/core-plot/tree/master/examples) for examples of some of its capabilities.


# This Project

This version of CorePlot is forked from the commit [8f4b2ee7b99d3fbe3bbae51ebe7dcc1106bd1d13](https://github.com/core-plot/core-plot/tree/8f4b2ee7b99d3fbe3bbae51ebe7dcc1106bd1d13) of the [original](https://github.com/core-plot/core-plot). This particular version contains some tweaks for CorePlot to work better with the [SmartWoundCare Project](https://bitbucket.org/cosmomeese/smartwoundcare/overview), namely this involves changing lots of the NSLog calls to DLog.